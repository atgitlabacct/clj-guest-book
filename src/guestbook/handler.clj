(ns guestbook.handler
  (:require [compojure.core :refer [defroutes routes]]
            [clojure.pprint]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.stacktrace :refer [wrap-stacktrace]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.session.memory :refer [memory-store]]
            [noir.session :as session]
            [noir.validation :refer [wrap-noir-validation]]
            [hiccup.middleware :refer [wrap-base-url]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [guestbook.routes.home :refer [home-routes]]
            [guestbook.routes.user :refer [user-routes]]
            [guestbook.routes.foo :refer [foo-routes]]
            [guestbook.routes.auth :refer [auth-routes]]
            [guestbook.models.db :as db]))

(defn init []
  (println "guestbook is starting")
  (if-not (.exists (java.io.File. "./db.sq3"))
    (do
      (println "Creating guestbook table")
      (db/create-guestbook-table))))

(defn destroy []
  (println "guestbook is shutting down"))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Sorry, but we can't find this shiiite, braaah"))

;; This doesn't work
(defn wrap-print-request-maps
  "Simply logs the request maps"
  [handler]
  (fn [request] 
    (clojure.pprint/pprint request)
    (handler request)))

(defn wrap-nocache
  "Send a header of Pragma no-cache"
  [handler]
  ;; Return a functin
  ;; Obviously a closure here because we can access the handler param
  ;; within the function; pulling scope with the function 
  (fn [request] 
    ;;(println "----" (handler request))
    (let [response (handler request)]
      (assoc-in response [:headers "Pragma"] "no-cache"))))

(defn wrap-dummy-route
  "Stupid route without compojure routing"
  [handler]
  (fn
    [request] 
    (let [method (get request :request-method)
          uri (get request :uri)]
      (println "Method: " method)
      ;; Curious why the uri here is diff
      (println "URI: "uri))
    (handler request)))

(def app
  (-> (routes auth-routes home-routes foo-routes user-routes app-routes)
      (handler/site)
      (wrap-dummy-route)
      (session/wrap-noir-session {:store (memory-store)})
      (wrap-stacktrace)
      (wrap-nocache)
      (wrap-base-url)
      (wrap-reload '(guestbook.routes.user 
                     guestbook.routes.foo 
                     guestbook.routes.auth 
                     guestbook.routes.home))
      #_(wrap-print-request-maps)
      (wrap-noir-validation)))
