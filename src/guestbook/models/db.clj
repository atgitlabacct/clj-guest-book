(ns guestbook.models.db
  ;; require to pull in the clojure namespaces and
  ;; :as to allow sql/foo
  (:require [clojure.java.jdbc :as sql])
  ;; Imort to reference the java class
  (:import java.sql.DriverManager))

(def db {:classname "org.sqlite.JDBC"
         :subprotocol "sqlite"
         :subname "db.sq3"})

(defn create-user-table
  []
  (sql/with-connection
    db
    (sql/create-table
      :users
      [:id "varchar(20) PRIMARY KEY"]
      [:pass "varchar(100)"])))

(defn add-user-record
  [user]
  (sql/with-connection db
    (sql/insert-record :users user)))

(defn get-user
  [id]
  (sql/with-connection db
    (sql/with-query-results
      res ["select * from users where id = ?" id] (first res))))

(defn create-guestbook-table
  "Pretty self explanatory"
  []
  (sql/with-connection ;; ensures the db conn is properly cleaned up after use
    db
    (sql/create-table
      :guestbook
      [:id "INTEGER PRIMARY KEY AUTOINCREMENT"]
      [:timestamp "TIMESTAMP DEFAULT CURRENT_TIMESTAMP"]
      [:name "TEXT"]
      [:message "TEXT"])
    (sql/do-commands "CREATE INDEX timestamp_index ON guestbook (timestamp)")))

(defn drop-guestbook-table
  []
  (sql/with-connection
    db
    (sql/drop-table :guestbook)))

(defn read-guests
  "Read guests and messages from the db"
  []
  (sql/with-connection
    db
    (sql/with-query-results res
      ["SELECT * FROM guestbook ORDER BY timestamp DESC"]
      (doall res))))

(defn save-message
  "Saves the message"
  [name message]
  (sql/with-connection
    db
    (sql/insert-values
      :guestbook
      [:name :message :timestamp]
      [name message (new java.util.Date)])))
