(ns guestbook.routes.user
  (:require [compojure.core :refer :all]
            [guestbook.views.layout :as layout]
            [hiccup.form :refer :all]))

(defn display-profile
  "Displays a users profiles"
  [id]
  (layout/common
    [:p "Place holder for a profile"]))

(defn display-settings
  "Displays the user settings"
  [id]
  (layout/common
    [:p "Place holder for settings"]))

(defn change-password
  "Change the user password"
  [id]
  (layout/common
    [:p "Place holder to change the password"]))

(defn map-request-to-string
  [r]
  (map
    (fn [arg]
      (let [[k v] arg]
        [:p (str k " - " v)]))
    (vec r)))

(defn display-foo
  "Display foo"
  [request]
  (layout/common
    (map-request-to-string request)))

(defroutes user-routes
  (context "/user/:id" [id]
           (GET "/profile" [] (display-profile id))
           (GET "/settings" [] (display-settings id))
           (GET "/change-password" [] (change-password id))
           (GET "/request" request  (display-foo request))
           (GET "/headers" {headers :headers} (display-foo headers))
           (GET "/remote_addr" {remote-addr :remote-addr
                                server-port :server_port} (str remote-addr ":" server-port))
           ;;(GET "/foo" request (interpose ", " (vals request)))
           ))
