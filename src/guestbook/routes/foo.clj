(ns guestbook.routes.foo
  (:require [compojure.core :refer :all]
            [guestbook.views.layout :as layout]
            [hiccup.form :refer :all]))

(defn foo-req
  "Handle the foo route"
  [id]
  (layout/common
    [:h1 "Handling foo request"]
    [:p (str "value of " id)]))

(defroutes foo-routes
  (GET "/foo" []  (foo-req "empty"))
  (GET "/foo/:id" [id] (foo-req id)))


