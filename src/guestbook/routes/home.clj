(ns guestbook.routes.home
  (:require [compojure.core :refer :all]
            [guestbook.views.layout :as layout]
            [hiccup.form :refer :all]
            [noir.session :as session]
            [guestbook.models.db :as db]))

(defn format-time
  [timestamp]
  (-> "dd/MM/yyyy"
      (java.text.SimpleDateFormat.)
      (.format timestamp)))

(defn show-guests
  "Shows a listing of guests"
  []
  [:ul.guests
   (for [{:keys [message name timestamp]} (db/read-guests)]
     [:li
      [:blockquote message]
      [:p "-" [:cite name]]
      [:time (format-time timestamp)]])])

(defn home [& [name message error]]
  (layout/common
    [:h1 "Guest book" (session/get :user)]
    [:p "Welcome to my guestbook"]
    [:p error]
    (show-guests)
    [:hr]
    (form-to [:post "/"]
             [:p "Name:"]
             (text-field "name" name)
             [:p "Message"]
             [:textarea {:rows 10 :cols 40 :name "message"}]
             (submit-button "comment"))))

(defn save-message
  "Check name and parameters are set then pring to console otherwise
  generate an error messaage"
  [name message]
  (cond
    (empty? name) (home name message "Some dummy forgot to leave a name")
    (empty? message) (home name message "Don't you wanna speak")
    :else (do
            (db/save-message name message)
            (home))))

(defroutes home-routes
  (GET "/" [] (home))
  (GET "/home" [] (home))
  (POST "/" [name message] (save-message name message)))
